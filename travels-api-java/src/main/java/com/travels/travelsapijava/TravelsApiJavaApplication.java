package com.travels.travelsapijava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class})
public class TravelsApiJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelsApiJavaApplication.class, args);
	}

}
