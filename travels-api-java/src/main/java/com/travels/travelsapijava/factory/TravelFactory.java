package com.travels.travelsapijava.factory;

import com.travels.travelsapijava.model.Travel;

public interface TravelFactory {
    Travel createTravel (String type);
}
