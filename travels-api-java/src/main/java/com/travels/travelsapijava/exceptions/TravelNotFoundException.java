package com.travels.travelsapijava.exceptions;

public class TravelNotFoundException extends Exception {

    private static final long serialVersionUID = -2586209354700102349L;

    public TravelNotFoundException(){
        super();
    }

    public TravelNotFoundException(String msg){
        super(msg);
    }

    public TravelNotFoundException(String msg, Throwable cause){
        super(msg, cause);
    }

}
