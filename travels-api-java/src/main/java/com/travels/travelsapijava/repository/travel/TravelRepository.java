package com.travels.travelsapijava.repository.travel;


import com.travels.travelsapijava.model.Travel;

import java.util.Optional;

public interface TravelRepository {
    Optional<Travel> findByOrderNumber(String orderNumber);
}
